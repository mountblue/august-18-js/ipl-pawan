module.exports = {
  matchesPerYear(matches) {
    const matchesPerYear = matches.reduce((newObject, element) => {
      if (newObject[element.season]) {
        newObject[element.season] += 1;
      } else {
        newObject[element.season] = 1;
      }
      return newObject;
    }, {});
    return matchesPerYear;
  },

  matchesWonPerYear(matches) {
    const years = matches.map(element => element.season);
    const year = new Set(years);
    const matchesWonPerYear = matches.reduce((newObject, element) => {
      if (element.winner !== '') {
        if (newObject[element.winner]) {
          if (newObject[element.winner][element.season]) {
            newObject[element.winner][element.season] += 1;
          } else {
            newObject[element.winner][element.season] = 1;
          }
        } else {
          const yearMatches = {};
          yearMatches[element.season] = 1;
          newObject[element.winner] = yearMatches;
        }
      }
      return newObject;
    }, {});
    Object.values(matchesWonPerYear).forEach((element) => {
      year.forEach((item) => {
        if (!element[item]) {
          element[item] = 0;
        }
      });
    });
    return matchesWonPerYear;
  },

  extraRunPerTeam(deliveries, matches) {
    let extraRun;
    const matchesId = matches.filter(match => match.season == 2016).map(element => element.id);

    const extraRunPerTeams = deliveries.reduce((newObject, element) => {
      if (matchesId.includes(element.match_id)) {
        if (newObject[element.bowling_team]) {
          extraRun = parseInt(element.extra_runs, 10);
          newObject[element.bowling_team] += extraRun;
        } else {
          extraRun = parseInt(element.extra_runs, 10);
          newObject[element.bowling_team] = extraRun;
        }
      }
      return newObject;
    }, {});
    return extraRunPerTeams;
  },

  topEconomicalBowler(deliveries, matches) {
    let totalRun;
    const matchesId = matches.filter(match => match.season == 2015).map(element => element.id);

    let topEconomicalBowler = deliveries.reduce((newObject, element) => {
      if (matchesId.includes(element.match_id)) {
        if (newObject[element.bowler]) {
          totalRun = parseInt(element.total_runs, 10);
          newObject[element.bowler] += totalRun;
        } else {
          totalRun = parseInt(element.total_runs, 10);
          newObject[element.bowler] = totalRun;
        }
      }
      return newObject;
    }, {});
    const sortArray = Object.entries(topEconomicalBowler);
    sortArray.sort((a, b) => a[1] - b[1]);
    topEconomicalBowler = sortArray.reduce((newObject, element, index) => {
      if (index >= sortArray.length - 10) {
        newObject[element[0]] = element[1];
      }
      return newObject;
    }, {});
    return topEconomicalBowler;
  },
};
