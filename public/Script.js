$.getJSON('./ipl.json', (data) => {
  const matchesPerYear = Object.entries(data.matchesPerYear);
  const extraRunPerTeam = Object.entries(data.extraRunPerTeam);
  const topEconomicalBowler = Object.entries(data.topEconomicalBowler);
  const year = Object.keys(data.matchesPerYear);
  const datas = Object.entries(data.matchesWonPerYear);
  const dataArray = datas.map(element => ({ name: element[0], data: Object.values(element[1]) }));
  const matchesWonPerYear = dataArray;

  Highcharts.chart('container1', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'matchesPerYear',
    },
    subtitle: {
      text: 'IPL-Data',
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: 'no of matches',
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: 'Population in 2017: <b>{point.y:.1f} millions</b>',
    },
    series: [{
      name: 'Season',
      data: matchesPerYear,
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        y: 10,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
    }],
  });

  Highcharts.chart('container2', {
    chart: {
      type: 'bar',
    },
    title: {
      text: 'matchesWonPerYear',
    },
    xAxis: {
      categories: year,
    },
    yAxis: {
      min: 0,
      title: {
        text: 'matchesWonPerYear',
      },
    },
    legend: {
      reversed: true,
    },
    plotOptions: {
      series: {
        stacking: 'normal',
      },
    },
    series: matchesWonPerYear,
  });

  Highcharts.chart('container3', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'extraRunPerTeam in 2016',
    },
    subtitle: {
      text: 'IPL-Data',
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: 'extraRun',
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: 'extraRunPerTeam in 2016: <b>{point.y:.1f} millions</b>',
    },
    series: [{
      name: 'Team',
      data: extraRunPerTeam,
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        y: 10,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
    }],
  });

  Highcharts.chart('container4', {
    chart: {
      type: 'column',
    },
    title: {
      text: 'topEconomicalBowlers in 2015',
    },
    subtitle: {
      text: 'IPL-Data',
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: 'run',
      },
    },
    legend: {
      enabled: false,
    },
    tooltip: {
      pointFormat: 'topEconomicalBowlers in 2015: <b>{point.y:.1f} millions</b>',
    },
    series: [{
      name: 'Bowlers',
      data: topEconomicalBowler,
      dataLabels: {
        enabled: true,
        rotation: -90,
        color: '#FFFFFF',
        align: 'right',
        y: 10,
        style: {
          fontSize: '13px',
          fontFamily: 'Verdana, sans-serif',
        },
      },
    }],
  });
});
