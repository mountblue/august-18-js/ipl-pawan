const expect = require('chai').expect;
const app = require('../ipl.js');

describe('count number of matches per season', () => {
  const input = [{ season: 2008 }, { season: 2009 }, { season: 2008 }, { season: 2009 }];
  const output = { 2008: 2, 2009: 2 };
  it('it should count matches per season', () => {
    expect(app.matchesPerYear(input)).to.deep.equal(output);
  });
});
describe('count matches win per season per team', () => {
  const input = [{ season: 2010, winner: 'CSK' }, { season: 2010, winner: 'CSK' }, { season: 2011, winner: 'CSK' },
    { season: 2010, winner: 'RR' }, { season: 2010, winner: 'RR' }, { season: 2009, winner: 'RR' }];
  const output = { CSK: { 2009: 0, 2010: 2, 2011: 1 }, RR: { 2009: 1, 2010: 2, 2011: 0 } };

  it('it should count matches won by team per year', () => {
    expect(app.matchesWonPerYear(input)).to.deep.equal(output);
  });
});

describe('Extra runs consided per team', () => {
  const input1 = [{ match_id: 601, bowling_team: 'team1', extra_runs: '5' },
    { match_id: 601, bowling_team: 'team2', extra_runs: '4' },
    { match_id: 602, bowling_team: 'team1', extra_runs: '3' }];
  const input2 = [{ id: 601, season: '2016' },
    { id: 602, season: '2017' }];

  const output = { team1: 5, team2: 4 };

  it('It should return extra runs consided per team', () => {
    expect(app.extraRunPerTeam(input1, input2)).to.deep.equal(output);
  });
});

describe('Top Economical Bowlers of 2015', () => {
  const input1 = [{ match_id: '534', bowler: 'player1', total_runs: '1' },
    { match_id: '535', bowler: 'player2', total_runs: '1' },
    { match_id: '536', bowler: 'player2', total_runs: '0' },
    { match_id: '537', bowler: 'player1', total_runs: '1' }];

  const input2 = [{ id: '534', season: '2015' }, { id: '535', season: '2015' },
    { id: '536', season: '2015' }, { id: '537', season: '2016' }];

  const output = { player1: 1, player2: 1 };


  it('It should return top economical bowlers of 2015', () => {
    expect(app.topEconomicalBowler(input1, input2)).to.deep.equal(output);
  });
});
