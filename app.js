const csvFilePathMatches = ('data/matches.csv');
const csvFilePathDeliveries = ('data/deliveries.csv');
const csv = require('csvtojson');
const fs = require('fs');
const ipl = require('./ipl.js');

csv().fromFile(csvFilePathMatches).then((matches) => {
  csv().fromFile(csvFilePathDeliveries).then((deliveries) => {
    const matchesPerYears = ipl.matchesPerYear(matches);
    const matchesWonPerYears = ipl.matchesWonPerYear(matches);
    const extraRunPerTeams = ipl.extraRunPerTeam(deliveries, matches);
    const topEconomicalBowlers = ipl.topEconomicalBowler(deliveries, matches);
    const iplJson = {
      matchesPerYear: matchesPerYears,
      matchesWonPerYear: matchesWonPerYears,
      extraRunPerTeam: extraRunPerTeams,
      topEconomicalBowler: topEconomicalBowlers,
    };
    fs.writeFile('./public/ipl.json', JSON.stringify(iplJson, null, 4), (err) => {
      if (err) {
        console.log('error');
      } else {
        console.log('data is added');
      }
    });
  });
});
