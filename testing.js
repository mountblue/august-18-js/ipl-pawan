
const csvFilePathMatches = ('data/matches.csv');
const csv = require('csvtojson');


csv().fromFile(csvFilePathMatches).then((matches) => {
  const years = matches.map(element => element.season);
  const mySet = new Set(years);
  mySet.forEach((element) => {
    console.log(element);
  });
  const matchesId = matches.filter(match => match.season == 2016).map(element => element.id);
  console.log(matchesId);
  const winnerTeam = matches.filter(match => match.season == 2016).map(element => element.winner);
  console.log(winnerTeam);
  // console.log(matches);
  const matchesInDelhi = matches.filter(match => match.city == 'Delhi').reduce(count => count + 1, 0);
  console.log(matchesInDelhi);
  const tossWinner = matches.filter(match => match.toss_winner == 'Deccan Chargers').filter(yearIs => yearIs.season == 2017).reduce(count => count + 1, 0);
  console.log(tossWinner);
});
